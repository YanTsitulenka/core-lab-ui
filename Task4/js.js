var elements;

$(function() {
    $.ajax({
        url: "http://www.json-generator.com/api/json/get/crfAiwPYmW?indent=2",
        type: "POST",
        dataType: "json",
        success: function(msg){
            displayBooks(msg);
            elements = msg;
        },
        error: function(msg){
            //alert('Sorry, ');
        }
    });

    $(document).on('click', '.book-buy', function() {
        var totalPrice = parseFloat($('.total-price').html());
        totalPrice += parseFloat($(this).parent().find('.price').html());
        $('.total-price').html(totalPrice.toFixed(2));
    });

    $(document).on('mouseenter', '.book-information', function () {
        $(this).css("opacity", "1");
        $(this).siblings('.book-img').animate({zoom: "120%", left: "-5%", top: "-5%"}, "slow");
    });

    $(document).on('mouseleave', '.book-information', function () {
        $(this).css("opacity", "0");
        $(this).siblings('.book-img').animate({zoom: "100%", left: "0%", top: "0%"}, "slow");
    });

    $('#AllButton').click(function () {
        displayBooks(elements);
    });

    $('#JSButton').click(function () {
        displayBooks(elements, 'js');
    });

    $('#CSSButton').click(function () {
        displayBooks(elements, 'css');
    });
});

function displayBooks(array, type) {
    $('.book').remove();
    if(!type || type == 'all') {
        for (var i = 0; i < array.length; i++) {
            $('#books').append('<div class="col-md-4 book">' +
                '<img class="book-img" src="' + array[i].image + '">' +
                '<div class="book-information">' +
                '<div class="book-header">' + array[i].name + '</div>' +
                '<div class="book-author">' + array[i].author + '</div>' +
                '<div class="book-intro">' + array[i].intro + '</div>' +
                '<div class="book-price">$<span class="price">' + array[i].price + '</span></div>' +
                '<div class="book-buy"><button type="button" class="btn btn-success">' +
                '<span class="glyphicon glyphicon-shopping-cart"></span> Buy</button></div>' +
                '</div>' +
                '</div>'
            );
        }
    } else {
        for (var i = 0; i < array.length; i++) {
            if (array[i].type == type) {
                $('#books').append('<div class="col-md-4 book">' +
                    '<img class="book-img" src="' + array[i].image + '">' +
                    '<div class="book-information">' +
                    '<div class="book-header">' + array[i].name + '</div>' +
                    '<div class="book-author">' + array[i].author + '</div>' +
                    '<div class="book-intro">' + array[i].intro + '</div>' +
                    '<div class="book-price">$<span class="price">' + array[i].price + '</span></div>' +
                    '<div class="book-buy"><button type="button" class="btn btn-success">' +
                    '<span class="glyphicon glyphicon-shopping-cart"></span> Buy</button></div>' +
                    '</div>' +
                    '</div>'
                );
            }
        }
    }
}


