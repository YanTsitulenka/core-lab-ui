function Product(make, model, count, price){
	this.make = make;
	this.model = model;
	this.count = count;
	this.price = price;
}

Product.prototype.getInfo = function(){
	console.log("-----\nMake: " + this.make + "\nModel: " + this.model + "\nCount: " + this.count + "\nPrice: $" + this.price);
};
Product.prototype.getTotalPrice = function(){
	return this.count * this.price;
};




function CellPhone(make, model, count, price, camera, memory){
	Product.call(this, make, model, count, price);
	this.camera = camera;
	this.memory = memory;
}

CellPhone.prototype = Object.create(Product.prototype);
CellPhone.prototype.constructor = CellPhone;
CellPhone.prototype.getInfo = function(){
	Product.prototype.getInfo.apply(this);
	console.log("Camera: " + this.camera + "\nMemory: " + this.memory);
};
CellPhone.prototype.getTotalPrice = function(){
	var basePrice = Product.prototype.getTotalPrice.apply(this);
	return basePrice + basePrice * this.memory/100;
};





function Printer(make, model, count, price, printSpeed, paperTrayCapacity){
	Product.call(this, make, model, count, price);
	this.printSpeed = printSpeed;
	this.paperTrayCapacity = paperTrayCapacity;
}

Printer.prototype = Object.create(Product.prototype);
Printer.prototype.constructor = Printer;
Printer.prototype.getInfo = function(){
	Product.prototype.getInfo.apply(this);
	console.log("Print speed: " + this.printSpeed + "\nPaper tray capacity: " + this.paperTrayCapacity);
};

function WashingMachine(make, model, count, price, maxLoad, spinSpeed){
	Product.call(this, make, model, count, price);
	this.maxLoad = maxLoad;
	this.spinSpeed = spinSpeed;
}

WashingMachine.prototype = Object.create(Product.prototype);
WashingMachine.prototype.constructor = WashingMachine;
WashingMachine.prototype.getInfo = function(){
	Product.prototype.getInfo.apply(this);
	console.log("Max load: " + this.maxLoad + "\nSpin speed: " + this.spinSpeed);
};





var cellPhone1 = new CellPhone("Apple", "Iphone 7", 7, 700, 16, 64);
var cellPhone2 = new CellPhone("Xiaomi", "Mi4", 10, 180,14, 16);
var cellPhone3 = new CellPhone("Samsung", "Note 7", 3, 610, 16, 32);

var printer1 = new Printer("Samsung", "ML4000", 2, 100, 10, 20);
var printer2 = new Printer("Epason", "XP323", 21, 157, 30, 30);
var printer3 = new Printer("HP", "P2035", 5, 200, 9, 40);

var washingMachine1 = new WashingMachine("BOSH", "WLG20260OE", 23, 200, 5, 1000);
var washingMachine2 = new WashingMachine("LG", "F1096ND3", 4, 280, 4, 1000);
var washingMachine3 = new WashingMachine("Indesit", "IWSB 5085", 13, 150, 5, 800);


var arr = [cellPhone1, cellPhone2, cellPhone3, printer1, printer2, printer3, washingMachine1, washingMachine2, washingMachine3];
for(var i=0; i<arr.length; i++){
	arr[i].getInfo();
	console.log("Total price: " + arr[i].getTotalPrice());
}
