import React from 'react';
import Information from './Information.jsx';
import { Link } from 'react-router'

class Employee extends React.Component {

    constructor(props) {
        super(props);
        if(Information.getEmployee(this.props.params.id)){
            this.state = {
                employee: Information.getEmployee(this.props.params.id)
            };
        }
    }


    render() {
        return (
            <div className="body">
                <div className="title">
                    Employee
                </div>
                <div className="employee-image">
                    <img src={`${this.state.employee['photo']}`} />
                </div>
                <div className="employee-name"><span>{this.state.employee['firstName']}  {this.state.employee['lastName']}</span></div>
                <div className="employee-job">{this.state.employee['jobPosition']}</div>
                <div className="employee-back-button">
                    <Link to="/" ><span className="glyphicon glyphicon-arrow-left"></span></Link>
                </div>
            </div>
        );
    }
}

export default Employee;