import React from 'react';
import {Modal, Button, ButtonToolbar, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import Information from './Information.jsx';

class ModalForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            show: false
        };
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.success = this.success.bind(this);

    }

    showModal() {
        this.setState({show: true});
    }

    hideModal() {
        this.setState({show: false});
    }

    success(){
        let employee = {
            firstName: document.getElementById('firstName').value,
            lastName: document.getElementById('secondName').value,
            jobPosition: document.getElementById('jobPosition').value,
            photo: 'https://freeiconshop.com/wp-content/uploads/edd/person-flat.png'
        };


        let imgFile = document.getElementById('file');
        if (imgFile.files && imgFile.files[0]) {
            let width;
            let height;
            let fileSize;
            let reader = new FileReader();
            reader.onload = function(event) {
                let dataUri = event.target.result,
                    img = document.createElement("img");
                img.src = dataUri;
                width = img.width;
                height = img.height;
                fileSize = imgFile.files[0].size;
                alert(width);
                alert(height);
                alert(fileSize);
            };
            reader.onerror = function(event) {
                console.error("File could not be read! Code " + event.target.error.code);
            };
            reader.readAsDataURL(imgFile.files[0]);
        }

        Information.addEmployee(employee);
        this.setState({show: false});

    }

    render() {
        return (
            <ButtonToolbar>
                <bottom className="add-bottom" onClick={this.showModal}>Add employee</bottom>

                <Modal
                    show={this.state.show}
                    onHide={this.hideModal}
                    dialogClassName="custom-modal"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-lg">Add Employee</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form id="employeeForm">
                            <FormGroup>
                                <ControlLabel>First Name</ControlLabel>
                                <FormControl
                                    id="firstName"
                                    type="text"/>
                            </FormGroup>
                            <FormGroup>
                                <ControlLabel>Last Name</ControlLabel>
                                <FormControl
                                    id="secondName"
                                    type="text"/>
                            </FormGroup>
                            <FormGroup>
                                <ControlLabel>Job Position</ControlLabel>
                                <FormControl
                                    id="jobPosition"
                                    type="text"/>
                            </FormGroup>
                            <FormGroup>
                                <ControlLabel>Photo</ControlLabel>
                                <FormControl
                                    id="file"
                                    type="file"
                                />
                            </FormGroup>
                            <Button bsStyle="success" onClick={this.success}>Save</Button>
                            <Button bsStyle="danger" className="close-button" onClick={this.hideModal}>Cancel</Button>
                        </form>
                    </Modal.Body>
                </Modal>
            </ButtonToolbar>
        );
    }
}


export default ModalForm;