import React from 'react';
import Information from './Information.jsx';
import { Link } from 'react-router'

class Contact extends React.Component {
    render() {
        return (
            <li className="contact">
                <img className="contact-image" src={`${this.props.photo}`} />
                <div className="contact-info">
                    <div className="contact-name">
                        <Link to={`/employee/${this.props.id}`} >{this.props.firstName} {this.props.lastName}</Link>
                    </div>
                    <div className="contact-job"> {this.props.jobPosition} </div>
                </div>
                <div className="contact-remove" onClick={(e) => Information.removeEmployee(this.props.id)}>
                    <span className="glyphicon glyphicon-trash"/>
                </div>
            </li>
        );
    }
}

export default Contact;