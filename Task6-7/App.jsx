import React from 'react';
import ContactList from './ContactList.jsx';
import Employee from './Employee.jsx';
import { Router, Route, browserHistory } from 'react-router'


class App extends React.Component {

    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={ContactList}/>
                <Route path="/employee/:id" component={Employee}/>
            </Router>
        );
    }
}

export default App;