import React from 'react';
import Contact from './Contact.jsx';
import Information from './Information.jsx';
import ModalForm from './ModalForm.jsx';

class ContactList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: Information.employees
        };
        this.handleSearch = this.handleSearch.bind(this);
    }


    handleSearch(event) {
        let searchQuery = event.target.value.toLowerCase();
        let employees = Information.employees.filter(function(el) {
            let searchValue = el.firstName.toLowerCase() + el.lastName.toLowerCase();
            return searchValue.indexOf(searchQuery) !== -1;
        });

        this.setState({
             employees: employees
        });

    }

    render() {
        return (
            <div className="body">
                <div className="title">
                   Employee Directory
                </div>
                <div className="functions">
                    <div className="search">
                        <input type="text" placeholder="Search..." className="search-field" onChange={this.handleSearch} />
                    </div>
                    <div id="add">
                        <ModalForm />
                    </div>
                </div>
                <div className="contacts">
                    <ul className="contacts-list">
                        {
                            this.state.employees.map(function(el, i) {
                                return <Contact
                                    key = {i}
                                    firstName = {el.firstName}
                                    lastName = {el.lastName}
                                    jobPosition = {el.jobPosition}
                                    photo = {el.photo}
                                    id = {i}
                                />;
                            })
                        }
                    </ul>
                </div>
            </div>
        );
    }
}
export default ContactList;