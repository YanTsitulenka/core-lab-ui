import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

class Information{
    static employees;

    static addEmployee(employee){
        this.employees.push(employee);
        localStorage.setItem("employees", JSON.stringify(this.employees));
        ReactDOM.render(<App />, document.getElementById('content'));
    }

    static getEmployee(id){
        return this.employees[id];
    }

    static setEmployees(){
        if(localStorage.getItem("employees")===null){
            this.employees = [
                {
                    firstName: 'First',
                    lastName: 'First',
                    jobPosition: 'CEO',
                    photo:'https://freeiconshop.com/wp-content/uploads/edd/person-flat.png'
                }, {
                    firstName: 'Second',
                    lastName: 'Second',
                    jobPosition: 'CEO',
                    photo:'https://freeiconshop.com/wp-content/uploads/edd/person-flat.png'
                }
            ];
            localStorage.setItem("employees",JSON.stringify(this.employees));
        } else {
            this.employees = JSON.parse(localStorage.getItem("employees"));
        }
    }
    static removeEmployee(id){
        this.employees.splice(id, 1);
        localStorage.setItem("employees", JSON.stringify(this.employees));
        ReactDOM.render(<App />, document.getElementById('content'));
    }
}

export default Information;