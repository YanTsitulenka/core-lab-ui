import React from 'react';
import ReactDOM from 'react-dom';
import Information from './Information.jsx';
import App from './App.jsx';

Information.setEmployees();

ReactDOM.render(<App />, document.getElementById('content'));

