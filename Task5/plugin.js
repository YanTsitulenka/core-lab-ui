;(function ( $, window, document, undefined ) {
    $.fn.gallery = function(options) {

        var speed = options['animationSpeed'];
        var dotBackground = options['color'];
        var gallery = $(this);
        var galleryImages = $('.img-gallery>img');

        gallery.wrap("<div id='slider-container'></div>");
        var width = $('#slider-container').width();
        var index = 0;
        var allSlidesLength = galleryImages.length;

        $('.img-gallery>img').width(width);
        gallery.width(width * allSlidesLength);


        gallery.css('left',-width);
        $('.img-gallery>img:last-child').prependTo('.img-gallery');


        $('#slider-container').append('<div class="prev"><span class="glyphicon glyphicon-chevron-left"></span></div>' +
            '<div class="next"><span class="glyphicon glyphicon-chevron-right"></span></div>' +
            '<div class="navigation"></div>');

        for (j=0; j < allSlidesLength; j++) {
            if (j==0) {
                $(".navigation").append('<div class="dot active"></div>');
                $(".navigation .active").css('background', dotBackground);
            }
            else {
                $(".navigation").append('<div class="dot"></div>');
            }
        }
        $(".navigation .dot").css('border-color', dotBackground);

        var dots = $(".navigation").children(".dot");


        $('.navigation .dot').click(function(){
            $(".navigation .active").css('background', 'none').removeClass('active');
            $(this).addClass('active');
            $(".navigation .active").css('background', dotBackground);

            var clickIndex = $(this).index();

            if(index < clickIndex) {
                var r = clickIndex-index;
                while (r){
                    $('.img-gallery>img:first-child').appendTo('.img-gallery');
                    r--;
                }
            } else if(clickIndex < index){
                var r = index-clickIndex;
                while (r){
                    $('.img-gallery>img:last-child').prependTo('.img-gallery');
                    r--;
                }
            }

            gallery.css('margin-left', 0);
            index = clickIndex;
        });



        function nextSlide() {
            gallery.animate({
                'margin-left':-width
            },speed, function() {
                $('.img-gallery>img:first-child').appendTo('.img-gallery');
                gallery.css('margin-left', 0);
            });

            if (index != allSlidesLength - 1) {
                index++;
            } else {
                index = 0;
            }

            setActiveDot();
        }

        function prevSlide() {
            gallery.animate({
                'margin-left':width
            },speed, function() {
                $('.img-gallery>img:last-child').prependTo('.img-gallery');
                gallery.css('margin-left', 0);
            });

            if (index != 0) {
                index--;
            } else {
                index = allSlidesLength - 1 ;
            }

            setActiveDot();
        }


        function setActiveDot() {
            $(".navigation .active").css('background', 'none').removeClass('active');
            $(dots[index]).addClass('active');
            $(".navigation .active").css('background', dotBackground);
        }

        $('.next').click(nextSlide);
        $('.prev').click(prevSlide)
    };
})( jQuery, window, document );
